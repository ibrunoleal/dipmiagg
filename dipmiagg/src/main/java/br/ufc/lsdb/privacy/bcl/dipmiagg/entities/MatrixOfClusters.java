package br.ufc.lsdb.privacy.bcl.dipmiagg.entities;

import java.util.ArrayList;
import java.util.List;

public class MatrixOfClusters {
	
	private Cluster[][] matrix;
	
	public MatrixOfClusters(int lines, int columns) {
		this.matrix = new Cluster[lines][columns];
	}
	
	public void addCluster(Cluster c, int i, int j) {
		this.matrix[i][j] = c;
	}
	
	public void addClusters(List<Cluster> clusters, int attributeIndex) {
		int i = 0;
		for (Cluster cluster : clusters) {
			addCluster(cluster, i, attributeIndex);
			i++;
		}
	}
	
	public List<Cluster> getClusters(int attributeIndex) {
		List<Cluster> clusters = new ArrayList<>();
		for (int i = 0; i < getNumberOfLines(); i++) {
			clusters.add(getCluster(i, attributeIndex));
		}
		return clusters;
	}
	
	public Cluster getCluster(int i, int j) {
		return matrix[i][j];
	}
	
	public int getNumberOfLines() {
		return matrix.length;
	}
	
	public int getNumberOfColumns() {
		return matrix[0].length;
	}
	
	public double getSensitivity(int attributeIndex) {
		double max = Double.NEGATIVE_INFINITY;
		double min = Double.POSITIVE_INFINITY;
		for (int i = 0; i < getNumberOfLines(); i++) {
			Cluster c = getCluster(i, attributeIndex);
			for (ClusterElement e : c.getElements()) {
				double tempValue = e.getValue();
				if(tempValue > max) {
					max = tempValue;
				}
				if(tempValue < min) {
					min = tempValue;
				}
			}
		}
		return Math.abs(max-min);
	}
	
	public String toString() {
		String res = "";
		/*
		 * TODO
		 */
		return res;
	}

}
