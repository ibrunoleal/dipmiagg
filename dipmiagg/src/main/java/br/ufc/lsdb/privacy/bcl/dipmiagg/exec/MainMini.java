package br.ufc.lsdb.privacy.bcl.dipmiagg.exec;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.ufc.lsdb.privacy.bcl.dipmiagg.entities.DataSet;
import br.ufc.lsdb.privacy.bcl.dipmiagg.entities.DiPMiAgg;
import br.ufc.lsdb.privacy.bcl.dipmiagg.entities.Tuple;

public class MainMini {

	public static void main(String[] args) {
		
		Double[] v1 = {5d,3d,1d};
		Tuple t1 = new Tuple(Arrays.asList(v1));
		Double[] v2 = {3d,1d,1d};
		Tuple t2 = new Tuple(Arrays.asList(v2));
		Double[] v3 = {2d,3d,1d};
		Tuple t3 = new Tuple(Arrays.asList(v3));
		Double[] v4 = {1d,1d,3d};
		Tuple t4 = new Tuple(Arrays.asList(v4));
		
		List<Tuple> listaDeTuplas = new ArrayList<>();
		listaDeTuplas.add(t1);
		listaDeTuplas.add(t2);
		listaDeTuplas.add(t3);
		listaDeTuplas.add(t4);
		
		DataSet dataset = new DataSet(listaDeTuplas);
		
		dataset.print();
		
		System.out.println("*****************************************************");
		
		DiPMiAgg diPMiAgg = new DiPMiAgg(dataset);
		int clusterSize = 2;
		double epsilon = 1.0;
		
		try {
			DataSet d = diPMiAgg.getAnonymisedDataset(clusterSize, epsilon);
			d.print();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
