package br.ufc.lsdb.privacy.bcl.dipmiagg.entities;

import java.util.ArrayList;
import java.util.List;

public class DataInicialization {
	
	private DataSet dataset ;

	private int clusterSize;

	public DataInicialization(DataSet dataset, int clusterSize) {
		this.dataset = dataset;
		this.clusterSize = clusterSize;
	}
	
	/**
	 * Builds a list of cluster elements ordered by the given attribute. 
	 * @param attributeIndex
	 * 		Attribute of reference for sort the list.
	 * @return
	 * 		An ordered list of cluster elementos based on the given attribute index.
	 */
	private List<ClusterElement> getOrderedList(int attributeIndex) {
		List<ClusterElement> list = new ArrayList<>();
		for (Tuple tuple : dataset.getTuples()) {
			ClusterElement c = new ClusterElement(tuple, attributeIndex);
			list.add(c);
		}
		list.sort(null);
		return list;
	}

	/**
	 * Builds the list of clusters for a given attribute.
	 * @param attributeIndex
	 * 		Index of the attribute used do build the clusters.
	 * @return
	 * 		A list of clusters for a given attribute.
	 */
	private List<Cluster> getClusters(int attributeIndex) {
		List<ClusterElement> orderedList = getOrderedList(attributeIndex);
		int start = 0;
		int end = this.clusterSize;
		List<Cluster> clusters = new ArrayList<>();
		while (start < orderedList.size()) {
			Cluster c = new Cluster();
			if (end > orderedList.size()) {
				end = orderedList.size();
			}
			for (int i = start; i < end; i++) {
				c.getElements().add(orderedList.get(i));
			}
			start = end;
			end = start + this.clusterSize;
			
			clusters.add(c);
		}
		return clusters;
	}
	
	/**
	 * Builds all microaggregate clusters.
	 * @return
	 * 		The matrix with all clusters, where each column j contains 
	 * 		the clusters for the j-th attribute of the dataset. 
	 */
	public MatrixOfClusters getMatrixOfClusters() {
		int lines = dataset.getSize() / this.clusterSize;
		int columns = dataset.getNumberOfAttributes();
		MatrixOfClusters m = new MatrixOfClusters(lines, columns);
		for (int j = 0; j < columns; j++) {
			m.addClusters(getClusters(j), j);
		}
		return m;
	}

}
