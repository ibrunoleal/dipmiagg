package br.ufc.lsdb.privacy.bcl.dipmiagg.exec;

import java.io.IOException;

import br.ufc.lsdb.privacy.bcl.dipmiagg.entities.DataSet;
import br.ufc.lsdb.privacy.bcl.dipmiagg.entities.DiPMiAgg;
import br.ufc.lsdb.privacy.bcl.dipmiagg.util.FileUtil;

public class MainFromFile {

	public static void main(String[] args) {
		
		String path = "/home/brunoleal/Desktop/dataset.csv";
		FileUtil fileUtil = new FileUtil(path);
		
		DataSet dataset = fileUtil.importFile();
		int clusterSize = 11;
		double epsilon = 1.0;
		
		dataset.print();
		
		System.out.println("*****************************************************");
		
		DiPMiAgg diPMiAgg = new DiPMiAgg(dataset);
		try {
			DataSet d = diPMiAgg.getAnonymisedDataset(clusterSize, epsilon);
			d.print();
			String pathOutput = "/home/brunoleal/Desktop/anonimised.txt";
			FileUtil util = new FileUtil(pathOutput);
			util.writeToFile(d);
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
}
