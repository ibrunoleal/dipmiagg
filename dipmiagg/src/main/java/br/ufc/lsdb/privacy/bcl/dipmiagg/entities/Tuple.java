package br.ufc.lsdb.privacy.bcl.dipmiagg.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * class for a register of the dataset.
 */
public class Tuple implements Cloneable {

	private List<Double> values;

	public Tuple(List<Double> values) {
		super();
		this.values = values;
	}
	
	public double getAttribute(int attributeIndex) {
		return values.get(attributeIndex);
	}
	
	public void setAttribute(int attributeIndex, double value) {
		values.set(attributeIndex, value);
	}
	
	public int getSize() {
		return values.size();
	}
	
	public Tuple clone() throws CloneNotSupportedException {
		List<Double> cValues = new ArrayList<>();
		for (double v : this.values) {
			cValues.add(v);
		}
		Tuple cTuple = new Tuple(cValues);
		return cTuple;
	}
	
	public String toString() {
		return this.values.toString();
	}
}
