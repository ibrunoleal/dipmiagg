package br.ufc.lsdb.privacy.bcl.dipmiagg.entities;

import org.apache.commons.math3.distribution.LaplaceDistribution;

public class LaplaceMechanism {
	
	public static double getNoise(double sensitivity, double epsilon) {
		double mean = 0;
		double scale_b = sensitivity/epsilon;
		LaplaceDistribution laplace = new LaplaceDistribution(mean, scale_b);
		return laplace.sample();
	}

	
}
