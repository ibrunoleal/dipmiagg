package br.ufc.lsdb.privacy.bcl.dipmiagg.entities;

public class ClusterElement implements Comparable<ClusterElement>, Cloneable{
	
	private Tuple tuple;
	
	private int indexedPosition;

	/**
	 * 
	 * @param tuple
	 * 		the entire tuple
	 * @param indexedPosition
	 * 		the index for the value of the tuple used to microaggregation clustering.
	 */
	public ClusterElement(Tuple tuple, int indexedPosition) {
		super();
		this.tuple = tuple;
		this.indexedPosition = indexedPosition;
	}

	@Override
	public int compareTo(ClusterElement o) {
		return Double.compare(this.tuple.getAttribute(indexedPosition), o.getTuple().getAttribute(indexedPosition));
	}

	public Tuple getTuple() {
		return tuple;
	}

	public void setTuple(Tuple tuple) {
		this.tuple = tuple;
	}

	public Double getValue() {
		return this.getTuple().getAttribute(indexedPosition);
	}
	
	public void setValue(double value) {
		this.getTuple().setAttribute(indexedPosition, value);
	}

	@Override
	public ClusterElement clone() throws CloneNotSupportedException {
		Tuple cTuple = this.tuple.clone();
		ClusterElement e = new ClusterElement(cTuple, indexedPosition);
		return e;
	}
	
	@Override
	public String toString() {
		return String.valueOf("(" + getValue() + ")");
	}	

}
