package br.ufc.lsdb.privacy.bcl.dipmiagg.util;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import br.ufc.lsdb.privacy.bcl.dipmiagg.entities.DataSet;
import br.ufc.lsdb.privacy.bcl.dipmiagg.entities.Tuple;

public class FileUtil {

	/**
	 * separation mark from a file line.
	 */
	public static final String REGEX_PADRAO = ",";
	
	/**
	 * path from the file to be loaded.
	 */
	private String path;
	
	/**
	 * 
	 * @param path
	 * 		path from the file to be loaded.
	 */
	public FileUtil(String path) {
		this.path = path;
	}
	
	/**
	 * Import a dataset from the file specified in the constructor. 
	 * @return
	 * 		The dataset from the file.
	 */
	public DataSet importFile() {
		File file = new File(path);
		List<Tuple> tuplesList = new ArrayList<>();
		try {
			LineIterator lineIterator = FileUtils.lineIterator(file);
			while(lineIterator.hasNext()) {
				List<Double> tupleValues = new ArrayList<>();
				
				String line = lineIterator.nextLine();
				String[] stringValues = line.split(REGEX_PADRAO);
				for (int i = 0; i < stringValues.length; i++) {
					tupleValues.add(Double.parseDouble(stringValues[i]));
				}
				
				Tuple t = new Tuple(tupleValues);
				tuplesList.add(t);
			}
			DataSet dataset = new DataSet(tuplesList);
			return dataset;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Writes the given dataset to a file specified by the path provided in the constructor.
	 * If the file already exists it will be merged at the end.
	 * @param dataset
	 * 		the dataset to be persisted in a file.
	 * @throws IOException
	 */
	public void writeToFile(DataSet dataset) throws IOException {
		File file = new File(path);
		for (Tuple t : dataset.getTuples()) {
			String line = t.toString() + "\n";
			String lineToWrite = line.replace("[", "").replaceAll("]", "").replaceAll(" ", "");
			FileUtils.writeStringToFile(file, lineToWrite, StandardCharsets.UTF_8, true);
		}
	}
	
}
