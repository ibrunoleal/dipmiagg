package br.ufc.lsdb.privacy.bcl.dipmiagg.entities;

/**
 * 
 */
public class DiPMiAgg {
	
	private DataSet dataset;

	public DiPMiAgg(DataSet dataset) {
		this.dataset = dataset;
	}
	
	/**
	 * anonymizes the dataset.
	 * @param clusterSize
	 * 		the number of elementos in each cluster for individual ranking microaggregation.
	 * @param epsilon
	 * 		the privacy budget for the laplace differential privacy mechanism. 
	 * @return
	 * 		the anonymized dataset.
	 * @throws CloneNotSupportedException
	 */
	public DataSet getAnonymisedDataset(int clusterSize, double epsilon) throws CloneNotSupportedException {
		DataSet cloneDataset = dataset.clone();
		DataInicialization dataInicialization = new DataInicialization(cloneDataset, clusterSize);
		MatrixOfClusters mc = dataInicialization.getMatrixOfClusters();
		for (int j = 0; j < mc.getNumberOfColumns(); j++) {
			double sensitivity = mc.getSensitivity(j);
			for (int i = 0; i < mc.getNumberOfLines(); i++) {
				Cluster c = mc.getCluster(i, j);
				double centroid = c.getCentroid();
				double noise = LaplaceMechanism.getNoise(sensitivity, epsilon);
				for (ClusterElement e : c.getElements()) {
					e.setValue(centroid+noise);
				}
			}
		}
		return cloneDataset;
	}
	
}
