package br.ufc.lsdb.privacy.bcl.dipmiagg.entities;

import java.util.ArrayList;
import java.util.List;

public class Cluster implements Cloneable{
	
	private List<ClusterElement> elements;

	public Cluster(List<ClusterElement> elements) {
		super();
		this.elements = elements;
	}
	
	public Cluster() {
		this.elements = new ArrayList<>();
	}

	public List<ClusterElement> getElements() {
		return elements;
	}

	public void setElements(List<ClusterElement> elements) {
		this.elements = elements;
	}
	
	public double getCentroid() {
		float sum = 0;
		for (ClusterElement e : this.elements) {
			sum += e.getValue(); 
		}
		return sum/this.elements.size();	
	}
	
	public boolean containsTuple(Tuple tuple) {
		for (ClusterElement e : elements) {
			if (e.getTuple().equals(tuple)) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public Cluster clone() throws CloneNotSupportedException {
		Cluster c = new Cluster();
		List<ClusterElement> cElements = new ArrayList<>();
		for (ClusterElement clusterElement : elements) {
			ClusterElement e = clusterElement.clone();
			cElements.add(e);
		}
		c.setElements(cElements);
		return c;
	}

	@Override
	public String toString() {
		return elements.toString();
	}

}
