package br.ufc.lsdb.privacy.bcl.dipmiagg.entities;

import java.util.ArrayList;
import java.util.List;

public class DataSet implements Cloneable {
	
	private List<Tuple> tuplas;
	
	public DataSet() {
		this.tuplas = new ArrayList<>();
	}

	public DataSet(List<Tuple> tuplas) {
		super();
		this.tuplas = tuplas;
	}
	
	public void addTuple(Tuple tuple) {
		this.tuplas.add(tuple);
	}
	
	public void setTuple(int index, Tuple tuple) {
		this.tuplas.set(index, tuple);
	}
	
	public Tuple getTuple(int index) {
		return this.tuplas.get(index);
	}
	
	public List<Tuple> getTuples() {
		return this.tuplas;
	}
	
	public int getSize() {
		return tuplas.size();
	}
	
	public int getNumberOfAttributes() {
		return tuplas.get(0).getSize();
	}
	
	@Override
	public DataSet clone() throws CloneNotSupportedException {
		List<Tuple> cTuples = new ArrayList<>();
		for (Tuple tuple : tuplas) {
			Tuple cTuple = tuple.clone();
			cTuples.add(cTuple);
		}
		DataSet cloneDataset = new DataSet(cTuples);
		return cloneDataset;
	}
	
	public void print() {
		for (Tuple tuple : tuplas) {
			System.out.println(tuple);
		}
	}

}
